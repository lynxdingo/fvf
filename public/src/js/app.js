import '../css/lib/bootstrap.min.css';
import '../css/lib/jquery-ui-lightness.1.12.1.css';
import '../css/album.css';
import '../css/navbar-top-fixed.css';

var products = {
    "name": "products",
    "genders": ["man", "woman"],
    "man": {
        "articles": ["jacket"],
        "jacket": [{
                "name": "Jacket - Buckweed",
                "color": "red",
                "brand": "S.Oliver"
            },
            {
                "name": "Jacket - Miles",
                "color": "green",
                "brand": "NoName"
            },
            {
                "name": "Jacket - Donut",
                "color": "blue",
                "brand": "Lewis"
            }
        ]
    },
    "woman": {}
};

function createHTML(productsData) {
    var rawTemplate = document.getElementById("manProductsTemplate").innerHTML;
    var compiledTemplate = Handlebars.compile(rawTemplate);
    var ourGeneratedHTML = compiledTemplate(productsData)

    var productsContainer = document.getElementById("products-container");
    productsContainer.innerHTML = ourGeneratedHTML;
}

createHTML(products);